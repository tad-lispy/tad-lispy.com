{
  description = "The personal website of Tad Lispy";

  inputs = {
    nixpkgs.url      = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url  = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        project-name = "tad-lispy.com";
        overlays = [  ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        buildInputs = [
        ];
        nativeBuildInputs = with pkgs; [
          zola
          gnumake
        ];
      in
      {
        devShell = pkgs.mkShell {
          inherit buildInputs nativeBuildInputs;
          name = "${project-name}-develpoment-shell";
          packages = [
            pkgs.jq
            pkgs.miniserve
          ];
          project_name = project-name; # Expose as an environment variable for make
        };
        defaultPackage = pkgs.stdenv.mkDerivation {
          name = project-name;
          version = "1.0.0";
          src = ./.;
          inherit buildInputs nativeBuildInputs;
        };
        packages.docker-image = pkgs.dockerTools.buildLayeredImage {
          name = "${project-name}-build-image";
          tag = "latest";
          created = "now";
          contents = buildInputs ++ nativeBuildInputs ++ [
            pkgs.bash
            pkgs.coreutils
            pkgs.git
            pkgs.httpie
            pkgs.cacert
          ];
          fakeRootCommands = ''
            mkdir --parents /tmp
          '';
          enableFakechroot = true;
          config.Cmd = [ "bash" ];
        };
      }
    );
}
