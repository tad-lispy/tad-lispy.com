---
title:      "Tad Lispy Social Club"
description: "A private network for nice people who like to learn together and support each other."
---

A private network for nice people who like to learn together and support each other.

  * Invitation only
  * Non-commercial
  * Dedicated to learning

Already a member? Head to [club.tad-lispy.com](https://club.tad-lispy.com/).

# Why join?

  * Meet other **nice people** with similar interests (**free software**[^1], **entrepreneurship**, **professional skills**).

  * Participate in a **bi-weekly live coding and learning sessions** and other events.

  * Get inspired, find **support**, share professional **opportunities** and business **ideas**.


# What's in it for me?

It's a semi-private space for me and people I like to stay in touch with. I don't like big tech social media (like LinkedIn, Xitter, Facebook and all that crap). Fediverse is nice, but it's very public. Some conversations are better to have among friends.


# How does it work

It's a private forum (we use [Discourse](https://www.discourse.org/)). Once invited, you can log in and participate in discussions organized by topic. You can also participate by email. The content is only visible to members, and I will only let nice people in. So it's nice.


# How to join?

Send me a message from your private email address (i.e. not your employer's address). I only invite people I know in real life, like my students or people I worked with.

[^1]: Free as in freedom, not just price. See <https://en.wikipedia.org/wiki/Free_software>.
