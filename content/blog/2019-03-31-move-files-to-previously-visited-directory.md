---
title: Is there an equivalent of `cd -` for `cp` or `mv`? 
---

Perhaps you know that in Unix shell you can go back and forth between current and previous directory using handy shortcut for the `cd` command:

``` sh
cd -
```

But this is `cd` specific. Is there a way to copy or move a file from current directory to the previous? Turns out there is. [Read more](https://superuser.com/q/1419624/195460).

