---
title: How to Test if a Given Value is a React.js Component 
---

> 2024 Update: Some links to external content are unfortunately outdated. The content in this post doesn't seem relevant to me anymore, except for archival reasons, so I'm not going to update those broken links.

It may seem trivial, but it's a bit tricky to test for that. The documentation for React's Test Utils[^broken-1] is lacking examples (as of the time of writing).

By comparing it with Virtual DOM glossary[^broken-2] one can assume that instantiating it with new keyword and then checking with `TestUtils.isCompositeComponent` is a way to go.

That's how I do it in CoffeScript:

``` coffee
it "is a react component", ->
  # ReactComponents can be instantiated with new keyword for test purposes
  # See https://facebook.github.io/react/docs/glossary.html#react-components
  expect (TestUtils.isCompositeComponent new CheckboxSelect)
    .to.be.ok
```

---

[^broken-1]: Broken link to https://facebook.github.io/react/docs/test-utils.html

[^broken-2]: Broken link to https://facebook.github.io/react/docs/glossary.html#react-components
