---
title: How to Install NPM Packages Written in CoffeeScript from a Git Repo
---

> 2024 Update: Some links to external content are unfortunately outdated. The content in this post doesn't seem relevant to me anymore, except for archival reasons, so I'm not going to update those broken links.

> 2017 update: Working on this problem eventually lead me to create [npm-git-install](https://github.com/lzrski/npm-git-install) helper. The program should not be needed since the introduction of [the `prepare` NPM script](https://docs.npmjs.com/cli/v10/using-npm/scripts#prepare-and-prepublish).

IDEA: Use npm `install` script with value same as `prepublish`, but this would have to be changed before publishing to registry.

It is said to be an anti-pattern

See ~~https://docs.npmjs.com/misc/scripts#common-uses~~ (broken link)

An issue by isaacs [run `prepublish` for git url packages](https://github.com/npm/npm/issues/3055)

Installing it from a directory (`../package/`) runs `prepublish`. WTF?

> IDEA: Make a gitinstall gulp task, which will:
> 
> -   npm install (to get dev dependencies)
> -   run build task

Would it be possible use .npmignore then?

The problem is that gulp is in dev-dependencies. So npm install has to be in install script, which seems a little strange.

Result: endless loop (npm install triggers install script; obviously).

> NOTE: Installing from local directory triggers prepublish

Maybe installing from git should work like that:

1.  clone to tmp dir
2.  install from tmp dir
3.  remove tmp dir

This doesn't seem very hard to do.
