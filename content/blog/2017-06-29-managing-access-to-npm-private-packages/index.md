---
title: Managing access to private NPM repositories
---

Make private package accessible to members of deployment team from command line:

``` sh
npm access grant read-only your-organisation:deployment @your-organisation/your-package
```
