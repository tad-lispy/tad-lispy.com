---
title: Shape Up by Ryan Singer from Basecamp
---

> 2024 Update: To large extent [Software Garden](https://software.garden/) was an attempt to implement the ideas from Singer's book, but adapted to working with clients.

Oh my! This book is the best. I want to work like that! You can [read the book here](https://basecamp.com/shapeup).
