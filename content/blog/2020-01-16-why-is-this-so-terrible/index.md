---
title: Why is `this` so terrible
---

Recently at a meetup in Utrecht I gave a short presentation: *Elements of Functional Programming for React.js Developers*. See [the slides](https://tad-lispy.gitlab.io/why-is-this-so-terrible/).
