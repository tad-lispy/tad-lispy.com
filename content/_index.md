---
description: "Personal website of Tad Lispy"
title:       "Tad Lispy"
extra: 
  links:
    - href: "club/"
      label: Club
    - href: "blog/"
      label: Blog
    - href: "https://gitlab.com/tad-lispy"
      label: GitLab
    - href: "https://github.com/tad-lispy"
      label: GitHub
    - href: "https://stackoverflow.com/users/1151982/tad-lispy?tab=profile"
      label: StackOverflow
    - href: "https://chaos.social/@lazurski"
      label: Mastodon
      rel:   me
    - href: "https://linkedin.com/in/lazurski"
      label: LinkedIn
---


**Hello**, my name is **Tad**. I'm a **web and mobile developer**.

![Tad Lispy logo](tad-lispy-centered.png "Tad Lispy a raketa") 

You can hire me through my company [Software Garden](https://software.garden/). We are based in the wonderful Amsterdam, the Netherlands.
